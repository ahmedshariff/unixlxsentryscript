# README #


### Installation ###

* Install python ([check here for windows installation instructions](https://www.howtogeek.com/197947/how-to-install-python-on-windows/)) (python version 3.0 or greater)
* Install [openpyxl](https://openpyxl.readthedocs.io/en/default/#installation)
* git/download the script

### Usage ###

* Just run the script from a command prompt/terminal. Eg:
  
```
#!python

python uniXLXSentryScript.py
```

* Follow instructions and provide information as prompted.
* Have fun

### Notes ###

* Works with xlxs files.

### In case of bugs ###

* You know where to find me *poker face*