import openpyxl as xl

PRESENT_STRING = 1
ABSENT_STRING = 0

def startInputs():
    inputFile = input("Input file: ")
    inputSheet = input("The name of the sheet to input: ")
    while True:
        startCoord = input("Starting cell of registration numbers (eg: B4): ")
        endCoord = input("The cell where the list of registration numbers end (eg: B87): ")
        
        startCoordTuple = xl.utils.coordinate_from_string(startCoord);
        endCoordTuple = xl.utils.coordinate_from_string(endCoord);
        
        if startCoordTuple[0].lower() != endCoordTuple[0].lower():
            print("Error: start and end cordinates of the list of registration numbers mustbe in the same column");
        elif startCoordTuple[1] >= endCoordTuple[1]:
            print("Warning: The provided starting cordinate is below the end coordinate, switching values");
            temp = startCoordTuple
            startCoordTuple = endCoordTuple
            endCoordTuple = temp
            temp = startCoord
            startCoordTuple = endCoord
            endCoordTuple = temp
            break
        else:
            break


    while True:
        attendenceOnly = input("Marking only attendance? (y/n): ")
        if attendenceOnly == "y" or attendenceOnly == "n":
            break
        else:
            print("Error: Invalid input")
    print("The values provided are as follows:\n\t Input file name: {0}\n\t Input sheet name: {1}\n\t Starting coordinate of registration numbers: {2}\n\t End cell coordinate of registration numbers: {3}\n\t Making attendenceOnly: {4}".format(inputFile,inputSheet,startCoord,endCoord,attendenceOnly.lower() == "y".lower()))
    while True:
        i = input("\nProceed? (y/n) ")
        if i.lower() == "y".lower():
            return((inputFile,
                    inputSheet,
                    startCoordTuple,
                    endCoordTuple,
                    attendenceOnly.lower() == "y".lower()));
        elif i.lower() == "n".lower():
            startInputs()
        else:
            print("Error: Invalid input")

def startApp():
    inputFile, inputSheet, startCoordTuple, endCoordTuple, attendenceOnly = startInputs()
    
    workBook = xl.load_workbook(inputFile)

    sheet = workBook.get_sheet_by_name(inputSheet)

    names = sheet.get_squared_range(xl.utils.column_index_from_string(startCoordTuple[0]),
                                startCoordTuple[1],
                                xl.utils.column_index_from_string(endCoordTuple[0]),
                                endCoordTuple[1])


    #print(sheet.get_squared_range(xl.utils.column_index_from_string('D'),4,xl.utils.column_index_from_#string('W'),87))
#    for c1 in #sheet.get_squared_range(xl.utils.column_index_from_string('D'),4,xl.utils.column_index_from_string#('W'),87):
#        print(c1)
#        for c in c1:
#            if c.value == "1" or c.value == "x" or c.value == 1:
#                val = 1
#            else:
#                val = 0
#            c.value = val
#            workBook.save(inputFile)
    nameDict = {}
    for n in names:
        nameDict[n[0].value] = (n[0].column, n[0].row);



    while True:
        print("Options: \n\t(q) quit\n\t(e) enter data \n\t(r) reset values\n\t(c) clear column")
        i = input("Enter choice: ")
        if i.lower() == "q".lower():
            return
        elif i.lower() == "r".lower():
            startApp()
            return
        elif i.lower() == "c".lower():
            markingColumn = input("The column to clear: ")
            for key in list(nameDict.keys()):
                sheet.cell(column = xl.utils.column_index_from_string(markingColumn), row = nameDict[key][1]).value = ""
            print("cleared column and saved file")
            workBook.save(inputFile)
        elif i.lower() != "e".lower():
            print("Error: invalid input")
        else:
        
            markingColumn = input("The column in which to mark values (eg: E): ")
            removeingString = ABSENT_STRING
            markingString = PRESENT_STRING
            if attendenceOnly:
                reversedEntry = input("Mark in reverse? (if yes, all entries will be marked as present, and the values entered here will be unmarked) (y/n) ")
                while True:
                    if reversedEntry.lower() == "y".lower():
                        print("\nNOTE: Reversed entry enabled")
                        removeingString = PRESENT_STRING
                        markingString = ABSENT_STRING
                        for key in list(nameDict.keys()):
                            sheet.cell(column = xl.utils.column_index_from_string(markingColumn), row = nameDict[key][1]).value = removeingString
                        break
                    elif reversedEntry.lower() == "n".lower():
                        removeingString = ABSENT_STRING
                        markingString = PRESENT_STRING
                        for key in list(nameDict.keys()):
                            sheet.cell(column = xl.utils.column_index_from_string(markingColumn), row = nameDict[key][1]).value = removeingString
                        break
                    else:
                        print("Invalid input")

            print("\nNOTE: To finsh input values enter \"q\"\nTo search registration number, you may provide only part of the registration number, eg: to searh s/1/123, you may enter \'123\'. To delete delete an entry, input \"d\"")

            print("Marking string: {0}".format(markingString))
            print("Removing string: {0}".format(removeingString))
            while True:
                remove = False
                duplicate = False
                found = False
                markingKey = None
                val = input("\nEnter value to search:")
                if val.lower() == "q".lower():
                    break
                elif val.lower() == "d".lower():
                    val = input("Enter entry to remove value from:")
                    remove = True;
                
                
                #print(attendenceOnly)
                for key in list(nameDict.keys()):
                    if val in key:
                        if found == True:
                            duplicate = True
                            break
                        markingKey = key
                        found = True
                if duplicate == True:
                    print("Mulitple entries for input {0}. Provide information to identify an entry uniquely.(eg: if input 123 has multiple entries, try 11/123 or s/11/123)".format(val))
                elif found == True:
                    if remove == True:
                        value = removeingString
                        print("Deleted value")
                    elif attendenceOnly == True:
                        value = markingString
                        print("Marked with \'{0}\'".format(markingString))
                    else:
                        value = input("Marks to enter: ")
                    sheet.cell(column = xl.utils.column_index_from_string(markingColumn) , row = nameDict[markingKey][1]).value = value
                else:
                    print("Could not find entry with given input")

            while True:
                i = input("Save File (y/n): ");
                if i.lower() == "y".lower():
                    workBook.save(inputFile)
                    print("File saved")
                    break
                elif i.lower() == "n":
                    break
                else:
                    print("Invalid input")


startApp()
        
